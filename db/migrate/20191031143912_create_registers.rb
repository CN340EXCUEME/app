class CreateRegisters < ActiveRecord::Migration[6.0]
  def change
    create_table :registers do |t|
      t.string :username
      t.string :password
      t.string :confirmpassword
      t.string :email

      t.timestamps
    end
  end
end
