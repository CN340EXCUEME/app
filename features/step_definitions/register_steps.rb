Given /^the following registers:$/ do |registers|
  Register.create!(registers.hashes)
end

When /^I delete the (\d+)(?:st|nd|rd|th) register$/ do |pos|
  visit registers_path
  within("table tr:nth-child(#{pos.to_i})") do
    click_link "Destroy"
  end
end

Then /^I should see the following registers:$/ do |expected_registers_table|
  rows=find("table").all('tr')
  table = rows.map{|r|r.all('th,td').map{|c|c.text.strip}}
  expected_registers_table.diff!(table)
end
