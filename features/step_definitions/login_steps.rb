Given /^the following logins:$/ do |logins|
  Login.create!(logins.hashes)
end

When /^I delete the (\d+)(?:st|nd|rd|th) login$/ do |pos|
  visit logins_path
  within("table tr:nth-child(#{pos.to_i})") do
    click_link "Destroy"
  end
end

Then /^I should see the following logins:$/ do |expected_logins_table|
   rows=find("table").all('tr')
  table = rows.map{|r|r.all('th,td').map{|c|c.text.strip}}
  expected_logins_table.diff!(table)
end
