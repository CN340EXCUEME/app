Feature: Manage registers
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new register
    Given I am on the new register page
    When I fill in "Username" with "username 1"
    And I fill in "Password" with "password 1"
    And I fill in "Confirmpassword" with "confirmpassword 1"
    And I fill in "Email" with "email 1"
    And I press "Create"
    Then I should see "username 1"
    And I should see "password 1"
    And I should see "confirmpassword 1"
    And I should see "email 1"

  Scenario: Delete register
    Given the following registers:
      |username|password|confirmpassword|email|
      |username 1|password 1|confirmpassword 1|email 1|
      |username 2|password 2|confirmpassword 2|email 2|
      |username 3|password 3|confirmpassword 3|email 3|
      |username 4|password 4|confirmpassword 4|email 4|
    When I delete the 3rd register
    Then I should see the following registers:
      |Username|Password|Confirmpassword|Email|
      |username 1|password 1|confirmpassword 1|email 1|
      |username 2|password 2|confirmpassword 2|email 2|
      |username 4|password 4|confirmpassword 4|email 4|
