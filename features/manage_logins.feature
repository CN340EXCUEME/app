Feature: Manage logins
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new login
    Given I am on the new login page
    When I fill in "Username" with "username 1"
    And I fill in "Password" with "password 1"
    And I press "Login"
    Then I should see "username 1"
    And I should see "password 1"

  Scenario: Delete login
    Given the following logins:
      |username|password|
      |username 1|password 1|
      |username 2|password 2|
      |username 3|password 3|
      |username 4|password 4|
    When I delete the 3rd login
    Then I should see the following logins:
      |Username|Password|
      |username 1|password 1|
      |username 2|password 2|
      |username 4|password 4|
